package mnemonic_test

import (
	"fmt"
	"log"

	"bitbucket.org/kccleoc/mnemonic"
)

func ExampleWalletSecret() {
	// create wallet seed according to BIP39
	wallet := mnemonic.GenWalletSecret(128, "!!PassPhrase!!")
	fmt.Println(wallet)

}

func ExampleRecoverFromMnemonic() {
	// recovery from mnemonic
	mne := "absent draw begin amused stand stool civil system bid genius tuna cram"
	wallet, err := mnemonic.RecoverFromMnemonic(mne, "MoreSecure")
	if err != nil {
		log.Fatalf("could not create wallet struct,%s", err)
	}
	fmt.Println(wallet)
}
