// Package mnemonic package is a tools that implemented BIP0039 standard
// It generates wallets from scratch and using mnemonic as recovery
// Also, recovery from mnemonic to seed is also supported
package mnemonic // import "bitbucket.org/kccleoc/mnemonic"

import (
	"crypto/rand"
	"crypto/sha256"
	"crypto/sha512"
	"errors"
	"fmt"
	"math/big"
	"strings"
	"unicode"

	"bitbucket.org/kccleoc/mnemonic/wordlist"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/pbkdf2"
)

var (
	zeroByte []byte
)

// WalletSecret define BIP39 HD WalletSeed master seed
type WalletSecret struct {
	secret    []byte
	mnemonic  []string
	entropy   []byte
	BIP39Seed []byte
}

// GenWalletSecret auto gen recovery mnemonic phrase with length
// according to the given bit, l [128 - 256bits]
// passphrase is default to begin with word "mnemonic"
//
// |  ENT  | CS | ENT+CS |  MS  |
//
// +-------+----+--------+------+
//
// |  128  |  4 |   132  |  12  |
//
// |  160  |  5 |   165  |  15  |
//
// |  192  |  6 |   198  |  18  |
//
// |  224  |  7 |   231  |  21  |
//
// |  256  |  8 |   264  |  24  |
//
func GenWalletSecret(l int, passphase string) *WalletSecret {

	// Generating the mnemonic with l bits of entropy
	mms, ent := genMnemonic(l)

	// Generating of seed [mnmonic sentence, "mnemonic" + passphrase]
	bs := mnemonicToSeed(mms, "mnemonic"+passphase)

	return &WalletSecret{
		secret:    []byte("Bitcoin seed"),
		mnemonic:  mms,
		entropy:   ent,
		BIP39Seed: bs,
	}
}

// Pretty Print WalletSecret struct whenever fmt.Print
func (hd *WalletSecret) String() string {

	return fmt.Sprintf(`
Wallet details (BIP39 implementation)
-------------------------------------
Mnemonic Phrase: %s
		
WalletSeed Seed (hex): [%x]
`, hd.mnemonic, hd.BIP39Seed)
}

// genMnemonic generate recovery mnemonic
// based on the entropy bit provided , 128 to 256 modulo 32
func genMnemonic(l int) ([]string, []byte) {

	// create entropy as byte
	entropy, _ := prng(l / 8)

	// checksum length, every 32bit, one cs bit
	csl := len(entropy) * 8 / 32

	// number of words in mnemonic, 11 bits for each word
	mml := (len(entropy)*8 + csl) / 11

	// ent + cs
	entNcs := appendCheckSum(entropy)

	return wordSlicer(entNcs, mml), entropy
}

// mnemonicToSeed implement BIP39 section "mnmonic to seed"
func mnemonicToSeed(w []string, p string) []byte {

	pwd := []byte(strings.Join(w, " "))

	salt := []byte(p)

	return pbkdf2.Key(pwd, salt, 2048, 64, sha512.New)
}

// prng generated random bytes (n bytes)
func prng(n int) ([]byte, error) {

	b := make([]byte, n)

	_, err := rand.Read(b)

	if err != nil {
		return nil, err
	}

	return b, nil
}

// appendCheckSum appends the first (len(entropy) / 32)bits
// of sha256(entropy) as checksum to entropy
func appendCheckSum(ent []byte) *big.Int {

	sum := sha256.Sum256(ent)

	csByte := sum[0] //max check sum bits is 8 bits in 256bit entropy

	// hacking bits
	// see https://play.golang.org/p/BAwyDbRPVQP
	// checksum length is in bytes so we divide by 4
	csBitLen := uint(len(ent) * 8 / 32)

	// get the checksum bits
	cs := getFirstFewBits(csByte, csBitLen)
	log.Debugf("check sum bits: %08b\t (len:%v)\n", cs, len(fmt.Sprintf("%b", cs)))

	// convert entropy to big number for easier bit hacking later
	entBig := big.NewInt(0).SetBytes(ent)
	log.Debug(ent)
	log.Debugf("Ent bit:\t\t%s\n", bitFmt(entBig, csBitLen*32))

	// first add zeros to end of entropy to reserve the cs space
	newEntBig := addTrailingZeroBits(entBig, csBitLen)

	// mask the reserved space with checksum bits, cs
	newEntBig = newEntBig.Xor(newEntBig, big.NewInt(cs))
	log.Debugf("Ent bit (with cs):\t%s\n", bitFmt(newEntBig, csBitLen*33))

	return newEntBig
}

// getFirstFewBits extract frist n bits from byte slice
// return as only the extracted bytes
func getFirstFewBits(b byte, n uint) int64 {

	//shift bytes based on n
	checksum := int64(b >> (8 - n))

	return checksum
}

// addTrailingZeroBits add n zero bits "0"
// to the end of slice of bytes
func addTrailingZeroBits(b *big.Int, n uint) *big.Int {

	//make bit "10"
	bit10 := big.NewInt(2)

	// add zero to end of bit by multiply bit "10") four time
	for i := 1; i <= int(n); i++ {
		b.Mul(b, bit10)
	}

	return b
}

// bitFmt is used to format debug output
func bitFmt(b *big.Int, i uint) string {
	return fmt.Sprintf("%0528b", b)[528-i:] +
		fmt.Sprintf(" (len: %v)", i)
}

// wordSlicer slices the entropy by 11bits and return
// slice of string [the words from bip wordlist]
func wordSlicer(e *big.Int, n int) []string {
	mneSentence := make([]string, n)

	// slice to words , 11 bit each
	for i := n - 1; i >= 0; i-- {
		// rightmost 11111111111
		word := big.NewInt(2047)

		// mask
		word.And(e, word)

		// shift bit to right by div, 2 ^ 11
		e.Div(e, big.NewInt(1<<11))

		// wordlist index
		idx := int(word.Uint64())
		log.Debugf("%11b\t(%v) word index %v", word, i, idx)

		// append words to slice
		mneSentence[i] = wordlistOrd()[idx]
	}

	return mneSentence
}

// wordlistOrd used to map the word with index number
func wordlistOrd() map[int]string {

	var m = make(map[int]string)

	for k, v := range strings.Split(wordlist.WordList, "\n") {
		m[k] = v
	}

	return m
}

// RecoverFromMnemonic create a *WalletSeed struct from
// given recovery Mnemonic (12 to 24 words long)
func RecoverFromMnemonic(mnemonic string, passphase string) (*WalletSecret, error) {

	// Generating the mnemonic with l bits of entropy
	ent, mnSlice, err := decodeMnemonic(mnemonic)
	if err != nil {
		return nil, errors.New("could not create Wallet seed")
	}

	// Generating of seed [mnmonic sentence, "mnemonic" + passphrase]
	bs := mnemonicToSeed(mnSlice, "mnemonic"+passphase)

	return &WalletSecret{
		secret:    []byte("Bitcoin seed"),
		mnemonic:  mnSlice,
		entropy:   ent,
		BIP39Seed: bs,
	}, nil
}

// decodeMnemonic return entropy of the
// recovery string according to the BIP39 wordlist
func decodeMnemonic(rcMme string) ([]byte, []string, error) {

	// field separator is non letter or non number
	// it is intentional to separate string using "SPACE" only
	// If using "return !unicode.IsLetter(c) && !unicode.IsNumber(c)"
	// all error like [zoo, zoo zoo zoo ] could be autocorrect
	// so "unicode.IsSpace(c)" is intented to throw error to api
	f := func(c rune) bool {
		// return !unicode.IsLetter(c) && !unicode.IsNumber(c)
		return unicode.IsSpace(c)
	}

	wordslice := strings.FieldsFunc(rcMme, f)

	// checking for validity - length
	switch len(wordslice) {
	case 12, 15, 18, 21, 24:
	default:
		return nil, nil,
			errors.New("cannot validate the length, must be 12,15,18,21,24 words")
	}

	// lookup to see if they are valid words in the wordlist
	// if not, loop break and return error
	revWordMap := wordlistRev()

	entCS := big.NewInt(0)

	bitSpace := big.NewInt(2048)

	for _, v := range wordslice {
		if idx, ok := revWordMap[v]; ok {
			bigAppend := big.NewInt(int64(idx))
			entCS.Mul(entCS, bitSpace)
			entCS.Xor(entCS, bigAppend)
		} else {
			return nil, nil,
				errors.New("cannot lookup one of the recovery words")
		}
	}

	// for every 32 bits , 1 Checksum bit at the end
	log.Debug(bitFmt(entCS, 132))
	csLen := uint(len(wordslice) * 11 / 33)
	// return entropy only, so shift bits
	entCS.Div(entCS, big.NewInt(1<<csLen))
	return entCS.Bytes(), wordslice, nil
}

// wordlistRev used to map the index with word
func wordlistRev() map[string]uint16 {

	var m = make(map[string]uint16)

	for k, v := range strings.Split(wordlist.WordList, "\n") {
		m[v] = uint16(k)
	}

	return m
}
